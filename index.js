// Importing modules
var express = require("express");
var mysql = require("mysql");

const app = express();
const PORT = 8000;

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root123",
  database: "Express_crud",
});

// Opem the mysql connection
const runAQuery = (query) => {
  con.connect((err) => {
    if (err) {
      console.log("Error connecting to Db");
      return;
    }
    console.log("Connection Established");
  });
  //To execute query
  con.query(query, function (err, result) {
    console.log(result);
    if (err) throw err;
    console.log("query executed!");
  });
};
// Function to create a database
const createDb = (dbname) => {
  return `CREATE DATABASE ${dbname}`;
};

// Function to specify database to be used
const useDb = (dbname) => {
  return `use ${dbname}`;
};

// Function to create a table
const createTable = (tablename, col1, col2, col3) => {
  return `CREATE TABLE ${tablename} (${col1} int(10) ,${col2} VARCHAR(10) , ${col3} int(5))`;
};

// Function to read the metadata of the table
const readTable = (tablename) => {
  return `DESC ${tablename}`;
};

// Function to add primarykey for a column
const addPrimaryKey = (tablename, constraint, colName) => {
  return `ALTER TABLE ${tablename} ADD CONSTRAINT ${constraint} PRIMARY KEY (${colName})`;
};

// Function to insert records
const insertRecords = (tablename, col1, col2, col3, value1, value2, value3) => {
  return `INSERT INTO ${tablename} (${col1},${col2},${col3}) VALUES (${value1},${value2},${value3})`;
};

// Function to read records
const readRecords = (tablename) => {
  return `SELECT * FROM ${tablename}`;
};

// Function to update records
const updateRecords = (tablename, updateCol, updateValue, keyCol, keyValue) => {
  return `UPDATE ${tablename} SET ${updateCol} = ${updateValue} WHERE ${keyCol} = ${keyValue}`;
};

// Function to delete records
const deleteRecords = (tablename, keyCol, keyValue) => {
  return `DELETE FROM ${tablename} WHERE ${keyCol} = ${keyValue}`;
};

// Function to delete Table
const deleteTable = (tablename) => {
  return `DROP TABLE ${tablename}`;
};

// Function to delete database
const deleteDatabase = (dbname) => {
  return `DROP DATABASE ${dbname}`;
};

app.post("/1", (req, res) => {
  res.send(runAQuery(createDb("movie_db")));
});

app.put("/2", (req, res) => {
  res.send(runAQuery(useDb("movie_db")));
});

app.post("/3", (req, res) => {
  res.send(runAQuery(createTable("movielist", "movie_id", "title", "year")));
});

app.get("/4", (req, res) => {
  res.send(runAQuery(readTable("movielist")));
});

app.put("/5", (req, res) => {
  res.send(runAQuery(addPrimaryKey("movielist", "Movie_key", "movie_id")));
});

app.post("/6", (req, res) => {
  res.send(
    runAQuery(
      insertRecords(
        "movielist",
        "movie_id",
        "title",
        "year",
        "23",
        "The Exorcist",
        "1977"
      )
    )
  );
});

app.get("/7", (req, res) => {
  res.send(runAQuery(readRecords("movielist")));
});

app.put("/8", (req, res) => {
  res.send(runAQuery(updateRecords("movielist", "year", "1973", "id", "23")));
});

app.delete("/9", (req, res) => {
  res.send(runAQuery(deleteRecords("movielist", "id", "23")));
});

app.delete("/10", (req, res) => {
  res.send(runAQuery(deleteTable("movielist")));
});

app.delete("/11", (req, res) => {
  res.send(runAQuery(deleteDatabase("movie_db")));
});

//Listening to port
app.listen(PORT, () => {
  console.log(`listening on port ${PORT}!`);
});
